package com.dke.pluralsight;

import android.Manifest;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Environment;
import android.support.annotation.RequiresPermission;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Dawid Krężel on 3/20/2016.
 */
public class Worker {

	private static final String TAG = "Worker";
	private final boolean mUseGpsToGetLocation = false;
	private final AppCompatActivity mActivity;

	public Worker(AppCompatActivity activity) {
		mActivity = activity;
	}

	@RequiresPermission(anyOf = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
	public Location getLocation() {
		Location lastLocation = null;

		if (mUseGpsToGetLocation) {
			LocationManager locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
			try {
				lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			} catch (SecurityException e) {
				Log.e(TAG, "No suitable permissions granted.");
			}
		}
		if (lastLocation == null) {
			lastLocation = createLocationManually();
		}

		simulateDelay();
		return lastLocation;
	}

	private Location createLocationManually() {
		Location location = new Location("fake");
		Date now = new Date();
		location.setTime(now.getTime());
		location.setLatitude(51.107885);
		location.setLongitude(17.038538);
		return location;
	}

	private void simulateDelay() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String reverseGeocode(Location location) {
		String addressDesc = null;

		Geocoder geocoder = new Geocoder(mActivity);
		try {
			List<Address> addressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
			if (!addressList.isEmpty()) {
				Address address = addressList.get(0);
				StringBuilder addressBuilder = new StringBuilder();
				for (int idx = 0; idx <= address.getMaxAddressLineIndex(); idx++) {
					if (idx != 0) {
						addressBuilder.append(", ");
						addressBuilder.append(address.getAddressLine(idx));
					}
				}
				addressDesc = addressBuilder.toString();
			}
		} catch (IOException e) {
			Log.e(TAG, "reverseGeocode", e);
		}
		return addressDesc;
	}

	public void save(Location location, String address, String filename) {
		File targetDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		assureThatDirectoryExists(targetDir);

		File outFile = new File(targetDir, filename);
		try {
			FileWriter fileWriter = new FileWriter(outFile, true);
			BufferedWriter writer = new BufferedWriter(fileWriter);

			String outLine = String.format("%s:%f/%f", DateFormat.getDateTimeInstance().format(location.getTime()), location.getLatitude(), location.getLongitude());
			writer.write(outLine);
			writer.write(address);

			writer.flush();
			writer.close();
			fileWriter.close();

		} catch (IOException e) {
			Log.e(TAG + ".save", e.getMessage());
		}
	}

	private void assureThatDirectoryExists(File directory) {
		if (!directory.exists()) {
			directory.mkdirs();
		}
	}
}
