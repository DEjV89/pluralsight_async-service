package com.dke.pluralsight;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

	private static final int LOCATION_REQUEST_PERMISSIONS_CODE = 1;

	@Bind(R.id.text_to_show)
	protected TextView mTextToShow;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
	}

	@OnClick(R.id.do_work_btn)
	public void doWork() {
		Worker worker = new Worker(this);
		mTextToShow.setText("Starting doWork");

		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
				&& ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission_group.LOCATION)) {
				Toast.makeText(this, "Need more rationale for permissions!", Toast.LENGTH_SHORT).show();
			} else {
				String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
				ActivityCompat.requestPermissions(this, permissions, LOCATION_REQUEST_PERMISSIONS_CODE);
			}
		}
		Location location = worker.getLocation();
		mTextToShow.setText("Retrieved Location");

		String address = worker.reverseGeocode(location);
		mTextToShow.setText("Retrieved Address");

		worker.save(location, address, "ResponsiveUx.out");
		mTextToShow.setText("doWork done");
	}

	@OnClick(R.id.exit_app_btn)
	public void exitApp() {
		finish();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
			case LOCATION_REQUEST_PERMISSIONS_CODE: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission granted
					Toast.makeText(MainActivity.this, "Permission granted", Toast.LENGTH_SHORT).show();
				} else {
					// permission denied
					Toast.makeText(MainActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
				}
				return;
			}
		}
	}
}
